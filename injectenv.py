import sys, os, re

if not len(sys.argv) or (sys.argv[0] == 'injectenv.py' and len(sys.argv) == 1):
    print("usage: python injectenv.py path_to_script")
    exit()
else:
    script = sys.argv[1] if sys.argv[0] == 'injectenv.py' else sys.argv[0]

with open(script, 'r') as file:
    build = file.read()
with open(script, 'w') as file:
    matches = re.findall(r"{(\w+)}", build)
    for match in set(matches):
        env = os.environ.get(match)
        if env:
            print("replacing {"+match+"} with "+env)
            build = build.replace('{'+match+'}', env)
    
    file.write(build)
