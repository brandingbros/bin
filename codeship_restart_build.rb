#!/home/rof/.rvm/rubies/ruby-2.1.2/bin/ruby

# modified restart script from the codeship VM which will wait for any current builds
# to complete before restarting

require 'json'
require 'net/http'
require 'timeout'
require 'pp'

class CodeshipRestartBuild
  def self.restart_build
    self.new.restart_build
  end

  def initialize
    @username = ENV['CODESHIP_USERNAME'] || abort("Please provide your API Key as an environment variable: CODESHIP_USERNAME=")
    @password = ENV['CODESHIP_PASSWORD'] || abort("Please provide your API Key as an environment variable: CODESHIP_PASSWORD=")
    @project_uuid = ENV['CODESHIP_PROJECT_UUID'] || abort("Please provide your project uuid as an environment variable: CODESHIP_PROJECT_UUID=")
    @auth = Authenticate.new.auth(@username, @password)
    @build = Build.new(@auth, @project_uuid)
  end

  def restart_build
    pp(@build.latest)
    abort
  
    # we wait for the build to be restarted
    message_sent = false
    wait_for = 10
    begin
      Timeout.timeout(wait_for * 60) do
        while JSON.parse(@project.project_data.body)['builds'].first['status'] == 'testing'
          if not message_sent
            print "waiting for current build to complete..."
            message_sent = true
          else
            print "."
          end
          sleep(5)
        end
      end
      
      print "\n" if message_sent
      
      if JSON.parse(@project.project_data.body)['builds'].first['status'] == 'testing'
        print "build already running, skipping"
        return
      end
      
      # if the build is waiting then no need to restart as a build will run soon
      if JSON.parse(@project.project_data.body)['builds'].first['status'] == 'waiting'
        print "there is a build already in queue, skipping"
        return
      end
      
      puts "restarting Build ##{build_to_restart.id} from commit #{build_to_restart.commit_id[0..7]} on #{@project.repository_name}"
      build_to_restart.restart
    rescue Timeout::Error
      puts "timeout (#{wait_for} minutes)"
    rescue NoMethodError
      puts "failed to restart build"
    end
  end

  def build_to_restart
    abort("We couldn't find any builds for project ##{@project_id}") if @project.builds.empty?
    Build.new(@project.builds.first, @api_key)
  end

  module HttpRequest
    def http_request
      http = Net::HTTP.new "api.codeship.com", 443
      http.use_ssl = true
      http.add_field 'Authorization: Bearer', @auth['access_token']
      http
    end
  end
  
  class Authenticate
    def auth(username, password)    
      uri = URI('https://api.codeship.com/v2/auth')

      req = Net::HTTP::Post.new(uri)
      req.basic_auth username, password

      res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true) do |http|
        http.request(req)
      end
      
      JSON.parse(res.body)
    end
  end

  class Build
    include HttpRequest

    def initialize(auth, projectUuid)
      @auth = auth
      @projectUuid = projectUuid
      @organizationUuid = (projectUuid == '76eaa2a0-d48c-0132-a2be-02473ab9142c' ? @auth['organizations'][0] : @auth['organizations'][1])['uuid']
    end
    
    def latest
      http_request.get("/v2/organizations/#{@organizationUuid}/projects/#{@projectUuid}/builds").first
    end

    def id
      @build_id
    end

    def commit_id
      @build['commit_id']
    end

    def restart
      http_request.post(url, "api_key=#{@api_key}").body
    end

    def url
      "/api/v1/builds/#{@build_id}/restart.json"
    end
  end
end

if $0 == __FILE__
  CodeshipRestartBuild.restart_build
end
