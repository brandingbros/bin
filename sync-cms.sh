set -x
cd /home/tbb/cms/site-app
sh pull.sh db/cms/{site}-content cms
sh pull.sh db/api/{site}-content api
sh pull.sh db/scheduler/{site}-content scheduler
sh pull.sh db/cms/{site}-data cms
sh pull.sh db/api/{site}-data api
sh pull.sh db/scheduler/{site}-data scheduler
ruby2.2 clear_cache.rb {site}