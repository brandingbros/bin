export CODESHIP_API_BRANCH={CI_BRANCH}
if [ "{CI_BRANCH}" = "develop" ] || [ "{CI_BRANCH}" = "master" ]; then
  # iabtrends
  export CODESHIP_PROJECT_UUID=ad7427b0-f019-0132-9264-3ee07e19cdb4
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 60
  # recorriendo
  export CODESHIP_PROJECT_UUID=77a136a0-4e02-0132-0fa3-1e6c3dad43cf
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 60
  # sudandola
  export CODESHIP_PROJECT_UUID=92b1ef20-2fb9-0132-30cc-02ab910b7901
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # psu
  export CODESHIP_PROJECT_UUID=ba274ad0-4e7b-0132-b13d-72c0305ed35c
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # belleza
  export CODESHIP_PROJECT_UUID=7f802cd0-4df2-0132-b86f-2294dbcebc54
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # mascotas
  export CODESHIP_PROJECT_UUID=a8d86100-8684-0132-8965-1636ae09f29e
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # saliendo
  export CODESHIP_PROJECT_UUID=7634b090-c7ca-0132-90d5-36013b770d7f
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # cookcina
  export CODESHIP_PROJECT_UUID=4bb4ff00-60f0-0132-c347-02eb9615503b
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # universitarios
  export CODESHIP_PROJECT_UUID=79d1c590-9df0-0132-13d4-0a6e67eb63d6
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # secundarios
  export CODESHIP_PROJECT_UUID=166f04e0-8d5b-0132-a9a4-5691319bff63
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # fucsia
  export CODESHIP_PROJECT_UUID=d7b0f6a0-5444-0132-ca9f-6274f9eea671
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
  sleep 120
  # carretes
  export CODESHIP_PROJECT_UUID=76eaa2a0-d48c-0132-a2be-02473ab9142c
  curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" https://services.postedin.com/api/codeship/projects/$CODESHIP_PROJECT_UUID/rebuild\?token\=$SERVICES_TOKEN
fi