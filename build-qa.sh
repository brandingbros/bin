set -x
source ~/.bash_profile
cd /home/tbb/{site}-builder
git checkout {CI_BRANCH} && \
git pull && \
git submodule update && \
(cd content && git pull && cd data && git pull) && \
(cd ../site-builder && git pull --recurse-submodules) && \
(cd ../global-data && git pull) && \
workon pelican && \
QA=true pelican --debug