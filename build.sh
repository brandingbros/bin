export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/Devel
source /usr/local/bin/virtualenvwrapper.sh

folder=""
debug=1
if [[ "{CI_BRANCH}" = "develop" ]]; then
    folder="qa"
    #debug=1
fi
cd $HOME/$folder/{site}-builder
git checkout {CI_BRANCH}
git pull
cd $HOME/$folder/site-builder
git checkout {CI_BRANCH}
git pull
workon pelican
[[ "{site}" == qa* ]] || make update site={site}
make publish site={site} DEBUG=$debug
